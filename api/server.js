const cors = require('cors');
const express = require('express');
const mysql = require('mysql');

const app = express();
app.use(cors());
const pool = mysql.createPool({
  host: process.env.MYSQL_HOST_IP,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DATABASE,
  
});

app.get('/', (req, res) => {
  return res.send({
        host: process.env.MYSQL_HOST_IP,
        user: process.env.MYSQL_USER,
        password: process.env.MYSQL_PASSWORD,
        database: process.env.MYSQL_DATABASE,
			apihost: process.env.REACT_APP_API_HOST,
	})
})

app.get('/test', (req, res) => {
  const { table } = req.query;

  pool.query(`select * from ${table}`, (err, results) => {
    if (err) {
      return res.send(err);
    } else {
      return res.send(results);
    }
  });
});

app.listen(process.env.REACT_APP_API_PORT, () => console.log(`App server now listening on port ${process.env.REACT_APP_API_PORT}`))